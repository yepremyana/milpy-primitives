from d3m import container
from milpy_primitives import bow
from common_primitives import dataset_to_dataframe, column_parser

data_uri = 'file:///Users/administrator/D3M/api/common-primitives/tests/data/datasets/multi_instance_dataset_1/datasetDoc.json'
dataset = container.Dataset.load(data_uri)

hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.get_hyperparams()
primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=hyperparams_class.defaults())
call_metadata = primitive.produce(inputs=dataset)
dataframe = call_metadata.value

hyperparams_class = column_parser.ColumnParserPrimitive.metadata.get_hyperparams()
primitive = column_parser.ColumnParserPrimitive(hyperparams=hyperparams_class.defaults().replace({'exclude_columns': [1, 2, 3]}))
call_metadata = primitive.produce(inputs=dataframe)
dataframe = call_metadata.value

hyperparams_class = bow.BOW.metadata.get_hyperparams()
primitive = bow.BOW(hyperparams=hyperparams_class.defaults())
primitive.set_training_data(inputs=dataframe)
primitive.fit()
call_metadata = primitive.produce(inputs=dataframe)
dataframe = call_metadata.value
print(dataframe)
dataframe.metadata.pretty_print()

