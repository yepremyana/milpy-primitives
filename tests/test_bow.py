import unittest
import pickle

from milpy_primitives import bow
from d3m.metadata import base as metadata_base
from d3m import container
from d3m.primitive_interfaces.base import PrimitiveBase
from d3m.exceptions import PrimitiveNotFittedError
from pandas.util.testing import assert_frame_equal

import os
from common_primitives import dataset_to_dataframe, column_parser

from sklearn.model_selection import cross_val_score as cross_validation
from MILpy.data import load_data
from sklearn.utils import shuffle
import random as rand

import pandas as pd
import numpy as np
from sklearn import metrics
from sklearn.utils import shuffle
import random as rand
from scipy.io import loadmat
import os, sys
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.metrics import accuracy_score

data_uri = 'file:///Users/alicery/Downloads/remove_this_later/datasets/seed_datasets_current/LL1_MIL_MUSK/LL1_MIL_MUSK_dataset/datasetDoc.json'

dataset = container.Dataset.load(data_uri)

hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.get_hyperparams()
primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=hyperparams_class.defaults())
call_metadata = primitive.produce(inputs=dataset)
dataframe = call_metadata.value

hyperparams_class = column_parser.ColumnParserPrimitive.metadata.get_hyperparams()
primitive = column_parser.ColumnParserPrimitive(hyperparams=hyperparams_class.defaults().replace({'exclude_columns': [1, 2, 3]}))
call_metadata = primitive.produce(inputs=dataframe)
dataframe = call_metadata.value
print(dataframe)

semantic_types_to_remove = set(['https://metadata.datadrivendiscovery.org/types/TrueTarget', 'https://metadata.datadrivendiscovery.org/types/SuggestedTarget'])
semantic_types_to_add = set(['https://metadata.datadrivendiscovery.org/types/PredictedTarget'])

class TestBOW(unittest.TestCase):
    def create_learner(self, hyperparams):
        clf = bow.BOW(hyperparams=hyperparams)
        return clf

    def set_training_data_on_learner(self, learner, **args):
        learner.set_training_data(**args)

    def fit_learner(self, learner: PrimitiveBase):
        learner.fit()

    def produce_learner(self, learner, **args):
        return learner.produce(**args)

    def basic_fit(self, hyperparams):
        learner = self.create_learner(hyperparams)
        training_data_args = self.set_data(hyperparams)
        self.set_training_data_on_learner(learner, **training_data_args)

        # self.assertRaises(PrimitiveNotFittedError, learner.produce, inputs=training_data_args.get("inputs"))

        self.fit_learner(learner)

        output = self.produce_learner(learner, inputs=training_data_args.get("inputs"))
        return output, learner, training_data_args

    def pickle(self, hyperparams):
        output, learner, training_data_args = self.basic_fit(hyperparams)

        # Testing get_params() and set_params()
        params = learner.get_params()
        learner.set_params(params=params)

        model = pickle.dumps(learner)
        new_clf = pickle.loads(model)
        new_output = new_clf.produce(inputs=training_data_args.get("inputs"))

        assert_frame_equal(new_output.value, output.value)

    def set_data(self, hyperparams):
        # hyperparams = hyperparams.get("use_semantic_types")
        # if hyperparams:
        return {"inputs": dataframe}
        # else:
        #     return {"inputs": train_bags,
        #             "outputs": train_bag_labels}

    def get_transformed_indices(self, learner):
        return learner._target_column_indices

    def new_return_checker(self, output):
        input_target = dataframe.select_columns(list([0,3]))
        for i in range(len(output.columns)):
            input_semantic_types = set(input_target.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            output_semantic_type = set(
                output.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            transformed_input_semantic_types = set(input_semantic_types) - semantic_types_to_remove
            transformed_input_semantic_types = transformed_input_semantic_types.union(semantic_types_to_add) if i==1 else transformed_input_semantic_types
            assert output_semantic_type == transformed_input_semantic_types

    def append_return_checker(self, output):
        for i in range(len(dataframe.columns)):
            input_semantic_types = set(dataframe.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            output_semantic_type = set(
                output.value.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            assert output_semantic_type == input_semantic_types

        self.new_return_checker(
            output.value.select_columns(list(range(len(dataframe.columns), len(output.value.columns)))))

    # def replace_return_checker(self, output):
    #     for i in range(len(dataframe.columns)):
    #         print(i)
    #         if i in [0,1,3]:
    #             continue
    #         input_semantic_types = set(dataframe.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
    #         output_semantic_type = set(
    #             output.value.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
    #         print(output_semantic_type)
    #         print(input_semantic_types)
    #         assert output_semantic_type == input_semantic_types
    #
    #     self.new_return_checker(output.value.select_columns(list([0,3])), [0,3])

    def test_with_semantic_types(self):
        hyperparams = bow.Hyperparams.defaults().replace({"use_semantic_types": True})
        self.pickle(hyperparams)

    def test_without_semantic_types(self):
        hyperparams = bow.Hyperparams.defaults()
        self.pickle(hyperparams)
    #
    def test_with_new_return_result(self):
        hyperparams = bow.Hyperparams.defaults().replace(
            {"return_result": 'new'})
        output, clf, _ = self.basic_fit(hyperparams)
        self.new_return_checker(output.value)

    def test_with_append_return_result(self):
        hyperparams = bow.Hyperparams.defaults().replace(
            {"return_result": 'append'})
        output, clf, _ = self.basic_fit(hyperparams)
        self.append_return_checker(output)

    # def test_with_replace_return_result(self):
    #     hyperparams = bow.Hyperparams.defaults().replace(
    #         {"return_result": 'replace', "use_semantic_types": True})
    #     output, clf, _ = self.basic_fit(hyperparams)
    #     print(output)
    #     # print(output.value.metadata.pretty_print())
    #     self.replace_return_checker(output)
    #
    def test_target_column_name(self):
        hyperparams = bow.Hyperparams.defaults().replace(
            {"return_result": 'append', "use_semantic_types": True})
        output, clf, _ = self.basic_fit(hyperparams)

        predicted_target_column_list = output.value.metadata.get_columns_with_semantic_type(
            'https://metadata.datadrivendiscovery.org/types/PredictedTarget')
        input_true_target_column_list = dataframe.metadata.get_columns_with_semantic_type(
            'https://metadata.datadrivendiscovery.org/types/SuggestedTarget')
        # Test if metadata was copied correctly
        predicted_target_column_metadata = output.value.metadata.select_columns(predicted_target_column_list)
        input_true_target_column_metadata = dataframe.metadata.select_columns(input_true_target_column_list)

        if len(predicted_target_column_list) == 1:
            # Checking that the predicted target name matches the input target
            predicted_name = predicted_target_column_metadata.query((metadata_base.ALL_ELEMENTS,)).get("name")
            input_true_target_name = input_true_target_column_metadata.query((metadata_base.ALL_ELEMENTS,)).get("name")
            assert predicted_name == input_true_target_name


if __name__ == '__main__':
    unittest.main()