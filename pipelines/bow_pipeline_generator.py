from d3m.metadata.pipeline import Pipeline, PrimitiveStep
from d3m.primitives.data_transformation.dataset_to_dataframe import Common as DatasetToDataFramePrimitive
from d3m.primitives.data_transformation.column_parser import Common as ColumnParserPrimitive
from d3m.primitives.data_transformation.construct_predictions import Common as ConstructPredictionsPrimitive
from d3m.metadata.base import ArgumentType, Context
from d3m.primitives.classification.instance_based_learning import MILpy as BOW


pipeline_description = Pipeline(context=Context.TESTING)
pipeline_description.add_input(name='inputs')

# # Step 1: DatasetToDataFrame
step_0 = PrimitiveStep(
    primitive_description=DatasetToDataFramePrimitive.metadata.query())
step_0.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
step_0.add_output('produce')
pipeline_description.add_step(step_0)

# Step 2: ColumnParser
step_1 = PrimitiveStep(primitive_description=ColumnParserPrimitive.metadata.query())
step_1.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_1.add_hyperparameter(name='exclude_columns', argument_type=ArgumentType.VALUE,
                          data=[1, 2, 3])
step_1.add_output('produce')
pipeline_description.add_step(step_1)

# Step 4: Primitive
step_3 = PrimitiveStep(primitive_description=BOW.metadata.query())
step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
step_3.add_output('produce')
pipeline_description.add_step(step_3)

# Step 5: ConstructPredictions
step_4 = PrimitiveStep(
    primitive_description=ConstructPredictionsPrimitive.metadata.query())
step_4.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')
step_4.add_argument(name='reference', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_4.add_output('produce')
pipeline_description.add_step(step_4)

# Final Output
pipeline_description.add_output(name='output predictions', data_reference='steps.3.produce')

with open("0ec4465c-0bf2-4ce7-9c57-28c1bb111ac1.json", "w") as fw:
    fw.write(pipeline_description.to_json(indent=4, sort_keys=True, ensure_ascii=False))