from typing import Any, Callable, List, Dict, Union, Optional, Sequence, Tuple
from numpy import ndarray
from collections import OrderedDict
from scipy import sparse
import os
import numpy
import typing
import pandas
import sklearn

# Custom import commands if any
from MILpy.Algorithms.BOW import BOW as bow

from d3m.container.numpy import ndarray as d3m_ndarray
from d3m.container import DataFrame as d3m_dataframe
from d3m.metadata import hyperparams, params, base as metadata_base
from d3m import utils
from d3m.base import utils as base_utils
from d3m.exceptions import PrimitiveNotFittedError
from d3m.primitive_interfaces.base import CallResult, DockerContainer

from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.unsupervised_learning import UnsupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import ProbabilisticCompositionalityMixin, ContinueFitMixin
from d3m import exceptions

Inputs = d3m_dataframe
Outputs = d3m_dataframe


class Params(params.Params):
    _logistic: Optional[sklearn.linear_model.LogisticRegression]
    _gauss_mix_model: Optional[sklearn.mixture.GaussianMixture]
    columns_to_drop: Optional[Sequence[Any]]

class Hyperparams(hyperparams.Hyperparams):
    k = hyperparams.Bounded[int](
        default=1,
        lower=1,
        upper=None,
        description='Number of words, the number of mixture components',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    covar_type = hyperparams.Enumeration[str](
        values=['diag', 'full', 'tied', 'spherical'],
        default='diag',
        description='Type of covariance matrix',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    max_iter = hyperparams.Bounded[int](
        default=100,
        lower=1,
        upper=None,
        description='The number of mixture components',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

    use_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to use as training input. If any specified column cannot be parsed, it is skipped.",
    )
    use_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to use as training target. If any specified column cannot be parsed, it is skipped.",
    )
    exclude_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not use as training inputs. Applicable only if \"use_columns\" is not provided.",
    )
    exclude_outputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not use as training target. Applicable only if \"use_columns\" is not provided.",
    )
    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        default='new',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should parsed columns be appended, should they replace original columns, or should only parsed columns be returned? This hyperparam is ignored if use_semantic_types is set to false.",
    )
    use_semantic_types = hyperparams.UniformBool(
        default=False,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Controls whether semantic_types metadata will be used for filtering columns in input dataframe. Setting this to false makes the code ignore return_result and will produce only the output dataframe"
    )
    error_on_no_input = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Throw an exception if no input column is selected/provided. Defaults to true to behave like sklearn. To prevent pipelines from breaking set this to False.",
    )


class BOW(UnsupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Primitive wrapping for Milpy BOW
    `Milpy documentation <https://github.com/jmarrietar/MILpy>`_

    """

    __author__ = "JPL MARVIN"
    metadata = metadata_base.PrimitiveMetadata({
        "algorithm_types": [metadata_base.PrimitiveAlgorithmType.INSTANCE_BASED_LEARNING, ],
        "name": "MILpy BOW",
        "primitive_family": metadata_base.PrimitiveFamily.CLASSIFICATION,
        "python_path": "d3m.primitives.classification.instance_based_learning.MILpy",
        "source": {'name': 'JPL', 'contact': 'mailto:alicery@jpl.nasa.gov',
                   'uris': ['https://gitlab.com/yepremyana/milpy-primitives/-/issues']},
        "version": "0.1.0",
        "id": "0ec4465c-0bf2-4ce7-9c57-28c1bb111ac1",
        "hyperparams_to_tune": ['k'],
        'installation': [
            {'type': metadata_base.PrimitiveInstallationType.PIP,
             'package_uri': 'git@gitlab.com:yepremyana/milpy-primitives.git@{git_commit}#egg=milpy-primitives'.format(
                 git_commit=utils.current_git_commit(os.path.dirname(__file__)),
             ),
             }]
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None,
                 _verbose: int = 0) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)

        self._clf = bow()

        self._inputs = None
        self._outputs = None
        self._training_inputs = None
        self._training_outputs = None
        self._columns_to_drop = None
        self._fitted = False
        self._new_training_data = False

    def set_training_data(self, *, inputs: Inputs) -> None:
        self._inputs = inputs
        self._fitted = False
        self._new_training_data = True

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        if self._inputs is None:
            raise ValueError("Missing training data.")

        attributes, targets = self._prepare_data(self._inputs)
        attributes = [attribute.values for attribute in attributes]
        self._clf.fit(
            attributes, targets, k=self.hyperparams['k'], covar_type=self.hyperparams['covar_type'],
            max_iter=self.hyperparams['max_iter']
        )
        self._fitted = True

        return CallResult(None)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        attributes_df, _ = self._prepare_data(inputs)
        attributes = [attribute.values for attribute in attributes_df]

        results = self._clf.predict(attributes)
        output = self._construct_predictions(inputs, attributes_df, results)
        outputs = base_utils.combine_columns(return_result=self.hyperparams['return_result'],
                                               add_index_columns=False,
                                               inputs=inputs, column_indices=[output.columns.get_loc(self._columns_to_drop[2])],
                                               columns_list=[output])
        return CallResult(outputs)

    def log_likelihoods(self) -> None:
        """
        A noop.
        """

        return None

    def _construct_predictions(self, input_dataframe, attributes_bags, predictions_bags):
        predictions_bags = predictions_bags.tolist()
        predictions_dataframe = None
        target_column = self._columns_to_drop[2]

        for i, group in enumerate(attributes_bags):
            group_df = d3m_dataframe(index=group.index)
            group_df[target_column] = predictions_bags[i] * len(group)

            if predictions_dataframe is None:
                predictions_dataframe = group_df
            else:
                predictions_dataframe = pandas.concat((predictions_dataframe, group_df))

        result = pandas.merge(input_dataframe['d3mIndex'], predictions_dataframe, left_index=True, right_index=True)
        result = d3m_dataframe(result, generate_metadata=True)

        for i in range(input_dataframe.metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']):
            column_name = input_dataframe.metadata.query((metadata_base.ALL_ELEMENTS, i))['name']
            if column_name == self._columns_to_drop[0]:
                result.metadata = result.metadata.update((metadata_base.ALL_ELEMENTS, 0,), input_dataframe.metadata.query((metadata_base.ALL_ELEMENTS, i)))
            if column_name == target_column:
                result.metadata = result.metadata.update((metadata_base.ALL_ELEMENTS, 1,), input_dataframe.metadata.query((metadata_base.ALL_ELEMENTS, i)))
                result.metadata = result.metadata.remove_semantic_type((metadata_base.ALL_ELEMENTS, 1),'https://metadata.datadrivendiscovery.org/types/SuggestedTarget')
                result.metadata = result.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 1),'https://metadata.datadrivendiscovery.org/types/PredictedTarget')
        return result

    def _prepare_data(self, input_dataframe):
        if not self._fitted:
            bag_id_idx = None
            index_column_idx = None
            target_idx = None
            for i in range(input_dataframe.metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']):
                if 'https://metadata.datadrivendiscovery.org/types/BagKey' in \
                        input_dataframe.metadata.query((metadata_base.ALL_ELEMENTS, i))['semantic_types']:
                    bag_id_idx = i
                if 'https://metadata.datadrivendiscovery.org/types/PrimaryKey' in \
                        input_dataframe.metadata.query((metadata_base.ALL_ELEMENTS, i))['semantic_types']:
                    index_column_idx = i
                if 'https://metadata.datadrivendiscovery.org/types/SuggestedTarget' in \
                        input_dataframe.metadata.query((metadata_base.ALL_ELEMENTS, i))['semantic_types'] or \
                        'https://metadata.datadrivendiscovery.org/types/Target' in \
                        input_dataframe.metadata.query((metadata_base.ALL_ELEMENTS, i))['semantic_types'] or \
                        'https://metadata.datadrivendiscovery.org/types/TrueTarget' in \
                        input_dataframe.metadata.query((metadata_base.ALL_ELEMENTS, i))['semantic_types']:
                    target_idx = i

                if bag_id_idx is not None and index_column_idx is not None and target_idx is not None:
                    break

            index_column = input_dataframe.columns[index_column_idx]
            group_column = input_dataframe.columns[bag_id_idx]
            target_column = input_dataframe.columns[target_idx]
            # TODO add this on the constructor.
            self._columns_to_drop = [index_column, group_column, target_column]

            # Swaroop drops it and we don't know why, so we just follow.
            if 'instanceID' in input_dataframe.columns:
                self._columns_to_drop.append('instanceID')

        attributes_bags = []
        labels_bags = []
        group_column = self._columns_to_drop[1]
        target_column = self._columns_to_drop[2]
        if not self._fitted:
            for group in input_dataframe.groupby(group_column):
                bag_df = group[1]
                labels_bags.append(bag_df[target_column].values[0])
                attributes_bags.append(bag_df.drop(columns=self._columns_to_drop))
        else:
            for group in input_dataframe.groupby(group_column):
                bag_df = group[1]
                attributes_bags.append(bag_df.drop(columns=self._columns_to_drop))

        return attributes_bags, labels_bags

    def get_params(self) -> Params:
        if not self._fitted:
            return Params(
                _logistic=None,
                _gauss_mix_model=None,
                columns_to_drop = self._columns_to_drop
            )

        return Params(
            _logistic=getattr(self._clf, '_logistic', None),
            _gauss_mix_model=getattr(self._clf, '_gauss_mix_model', None),
            columns_to_drop=self._columns_to_drop
        )

    def set_params(self, *, params: Params) -> None:
        self._clf._logistic = params['_logistic']
        self._clf._gauss_mix_model = params['_gauss_mix_model']
        self._columns_to_drop = params['columns_to_drop']

        if params['_logistic'] is not None:
            self._fitted = True
        if params['_gauss_mix_model'] is not None:
            self._fitted = True


    @classmethod
    def _get_columns_to_fit(cls, inputs: Inputs, hyperparams: Hyperparams):
        if not hyperparams['use_semantic_types']:
            return inputs, list(range(len(inputs.columns)))

        inputs_metadata = inputs.metadata

        def can_produce_column(column_index: int) -> bool:
            return cls._can_produce_column(inputs_metadata, column_index, hyperparams)

        columns_to_produce, columns_not_to_produce = base_utils.get_columns_to_use(inputs_metadata,
                                                                                   use_columns=hyperparams[
                                                                                       'use_inputs_columns'],
                                                                                   exclude_columns=hyperparams[
                                                                                       'exclude_inputs_columns'],
                                                                                   can_use_column=can_produce_column)
        return inputs.iloc[:, columns_to_produce], columns_to_produce
        # return columns_to_produce

    @classmethod
    def _can_produce_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int,
                            hyperparams: Hyperparams) -> bool:
        column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        accepted_structural_types = (int, float, numpy.integer, numpy.float64)
        accepted_semantic_types = set()
        accepted_semantic_types.add("https://metadata.datadrivendiscovery.org/types/Attribute")
        if not issubclass(column_metadata['structural_type'], accepted_structural_types):
            return False

        semantic_types = set(column_metadata.get('semantic_types', []))

        if len(semantic_types) == 0:
            cls.logger.warning("No semantic types found in column metadata")
            return False
        # Making sure all accepted_semantic_types are available in semantic_types
        if len(accepted_semantic_types - semantic_types) == 0:
            return True

        return False

    @classmethod
    def _get_targets(cls, data: d3m_dataframe, hyperparams: Hyperparams):
        if not hyperparams['use_semantic_types']:
            return data, list(data.columns), list(range(len(data.columns)))

        metadata = data.metadata

        def can_produce_column(column_index: int) -> bool:
            accepted_semantic_types = set()
            accepted_semantic_types.add("https://metadata.datadrivendiscovery.org/types/TrueTarget")
            column_metadata = metadata.query((metadata_base.ALL_ELEMENTS, column_index))
            semantic_types = set(column_metadata.get('semantic_types', []))
            if len(semantic_types) == 0:
                cls.logger.warning("No semantic types found in column metadata")
                return False
            # Making sure all accepted_semantic_types are available in semantic_types
            if len(accepted_semantic_types - semantic_types) == 0:
                return True
            return False

        target_column_indices, target_columns_not_to_produce = base_utils.get_columns_to_use(metadata,
                                                                                             use_columns=hyperparams[
                                                                                                 'use_outputs_columns'],
                                                                                             exclude_columns=
                                                                                             hyperparams[
                                                                                                 'exclude_outputs_columns'],
                                                                                             can_use_column=can_produce_column)
        targets = []
        if target_column_indices:
            targets = data.select_columns(target_column_indices)
        target_column_names = []
        for idx in target_column_indices:
            target_column_names.append(data.columns[idx])
        return targets, target_column_names, target_column_indices
